
import os

class Auth:
    """Google Project Credentials"""
    CLIENT_ID = ('196065796300-bbuho2e0an39u6ihjl1v4gqc7gj6m8qa.apps.googleusercontent.com')
    CLIENT_SECRET = 'cgIWZUrdAw2hSEMEZQ0_Msda'
    REDIRECT_URI = 'https://apress.herokuapp.com/gcallback'
    AUTH_URI = 'https://accounts.google.com/o/oauth2/auth'
    TOKEN_URI = 'https://accounts.google.com/o/oauth2/token'
    USER_INFO = 'https://www.googleapis.com/userinfo/v2/me'
    SCOPE = ['profile', 'email']

class Config:
    """Base config"""
    APP_NAME = "Aspire"
    SECRET_KEY = os.environ.get("SECRET_KEY") or "somethingsecret"


class DevConfig(Config):
    """Dev config"""
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'postgresql://rahulyadav:170923@localhost:5432/test'



class ProdConfig(Config):
    """Production config"""
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']

config = {
    "dev": DevConfig,
    "prod": ProdConfig,
}
