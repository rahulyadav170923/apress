from flask_login import UserMixin
from app import db
import 	datetime

class User(db.Model,UserMixin):
	id = db.Column(db.Integer, primary_key=True)
	email = db.Column(db.String(100), unique=True, nullable=False)
	name = db.Column(db.String(100), nullable=True)
	avatar = db.Column(db.String(200))
	tokens = db.Column(db.Text)
	created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow())    



class Post(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	text = db.Column(db.String(1000))
	time = db.Column(db.String(100))
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
