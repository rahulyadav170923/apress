
import os
import json
from flask import Flask, url_for, redirect, \
    render_template, session, request
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, login_required, login_user, \
    logout_user, current_user
from requests_oauthlib import OAuth2Session
from requests.exceptions import HTTPError
from config import config,Auth

basedir = os.path.abspath(os.path.dirname(__file__))


app = Flask(__name__)
app.config.from_object(config['prod'])
db = SQLAlchemy(app)
from models import User,Post
login_manager = LoginManager(app)
login_manager.login_view = "login"
login_manager.session_protection = "strong"

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))
""" OAuth Session creation """


def get_google_auth(state=None, token=None):
    if token:
        return OAuth2Session(Auth.CLIENT_ID, token=token)
    if state:
        return OAuth2Session(
            Auth.CLIENT_ID,
            state=state,
            redirect_uri=Auth.REDIRECT_URI)
    oauth = OAuth2Session(
        Auth.CLIENT_ID,
        redirect_uri=Auth.REDIRECT_URI,
        scope=Auth.SCOPE)
    return oauth


@app.route('/')
@login_required
def index():
	return render_template('index.html')

@app.route('/login')
def login():
    if current_user.is_authenticated():
    	return redirect(url_for('index'))
    google = get_google_auth()
    auth_url, state = google.authorization_url(
        Auth.AUTH_URI, access_type='offline')
    session['oauth_state'] = state
    return render_template('login.html',auth_url=auth_url)


@app.route('/gcallback')
def callback():
    if current_user is not None and current_user.is_authenticated():
        return redirect(url_for('index'))
    if 'error' in request.args:
        if request.args.get('error') == 'access_denied':
            return 'You denied access.'
        return 'Error encountered.'
    if 'code' not in request.args and 'state' not in request.args:
        return redirect(url_for('login'))
    else:
        google = get_google_auth(state=session['oauth_state'])
        try:
            token = google.fetch_token(
                Auth.TOKEN_URI,
                client_secret=Auth.CLIENT_SECRET,
                authorization_response=request.url)
        except HTTPError:
            return 'HTTPError occurred.'
        google = get_google_auth(token=token)
        resp = google.get(Auth.USER_INFO)
        if resp.status_code == 200:
            user_data = resp.json()
            email = user_data['email']
            user = User.query.filter_by(email=email).first()
            if user is None:
                user = User()
                user.email = email
            user.name = user_data['name']
            user.tokens = json.dumps(token)
            user.avatar = user_data['picture']
            db.session.add(user)
            db.session.commit()
            login_user(user)
            print 'token added'
            return redirect(url_for('index'))
        return 'Could not fetch your information.'

@app.route('/add_post')
@login_required
def add_post():
    data=Post()
    data.text = request.args.get('post')
    data.time = request.args.get('time')
    data.user_id = current_user.id
    db.session.add(data)
    db.session.commit()
    return "data entered"

@app.route('/logs')
@login_required
def logs():
    posts = Post.query.filter_by(user_id=current_user.id).all()
    return render_template('logs.html',posts=posts)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))

